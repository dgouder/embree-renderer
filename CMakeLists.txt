project(embreerenderer)

cmake_minimum_required(VERSION 3.0)

enable_language(CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)

# -------------------------------------------------------------
# lodepng
set(util_libs utils)
include_directories(lodepng/)
add_library(utils STATIC lodepng/lodepng.cpp)
# -------------------------------------------------------------
# embree

SET(EMBREE_MAX_ISA "SSE4.2" CACHE STRING "Selects highest ISA to support.")

set(EMBREE_STATIC_LIB ON CACHE BOOL "Build Embree as a static library." FORCE)
set(EMBREE_ISPC_SUPPORT OFF CACHE BOOL "Build Embree with support for ISPC applications." FORCE)
set(EMBREE_TUTORIALS OFF CACHE BOOL "Enable to build Embree tutorials" FORCE)
set(EMBREE_STAT_COUNTERS OFF CACHE BOOL "Enables statistic counters." FORCE)
set(EMBREE_RAY_MASK OFF CACHE BOOL "Enables ray mask support." FORCE)
set(EMBREE_BACKFACE_CULLING OFF CACHE BOOL "Enables backface culling." FORCE)
set(EMBREE_INTERSECTION_FILTER ON CACHE BOOL "Enables intersection filter callback." FORCE)
set(EMBREE_INTERSECTION_FILTER_RESTORE ON CACHE BOOL "Restores previous hit when hit is filtered out." FORCE)
set(EMBREE_TASKING_SYSTEM "INTERNAL" CACHE STRING "Selects tasking system" FORCE)
set(EMBREE_STATIC_RUNTIME OFF CACHE BOOL "Use the static version of the C/C++ runtime library." FORCE)
add_subdirectory(embree-3.1)
add_definitions(-DEMBREE_STATIC_LIB=1)

set(core_libs embree)

include_directories(embree-3.1/include)

# -------------------------------------------------------------

add_executable(solstice main.cpp)

target_link_libraries(solstice ${core_libs} ${util_libs})