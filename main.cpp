#include <iostream>

#include <embree3/rtcore.h>

#include "embree-3.1/common/math/vec3.h"

#include "lodepng/lodepng.h"

// Ray definition ninjaed from Embree samples
struct Ray
{
    embree::Vec3fa origin;
    embree::Vec3fa direction;

    float tfar;          //!< End of ray segment
    unsigned int mask;   //!< used to mask out objects during traversal
    unsigned int id;     //!< ray ID
    unsigned int flags;  //!< ray flags

    embree::Vec3f Ng;     //!< Not normalized geometry normal
    float u;              //!< Barycentric u coordinate of hit
    float v;              //!< Barycentric v coordinate of hit
    unsigned int primID;  //!< primitive ID
    unsigned int geomID;  //!< geometry ID
    unsigned int instID;  //!< instance ID

    __forceinline Ray(const embree::Vec3fa &org, const embree::Vec3fa &dir,
                      float tnear = embree::zero, float tfar = embree::inf,
                      float time = embree::zero, int mask = -1,
                      unsigned int geomID = RTC_INVALID_GEOMETRY_ID,
                      unsigned int primID = RTC_INVALID_GEOMETRY_ID,
                      unsigned int instID = RTC_INVALID_GEOMETRY_ID)
        : origin(org, tnear),
          direction(dir, time),
          tfar(tfar),
          mask(mask),
          primID(primID),
          geomID(geomID),
          instID(instID)
    {
    }
};

__forceinline RTCRay *RTCRay_(Ray &ray)
{
    return (RTCRay *)&ray;
}

__forceinline RTCRayHit *RTCRayHit_(Ray &ray)
{
    return (RTCRayHit *)&ray;
}

struct Vertex
{
    float x, y, z;
};

struct Triangle
{
    int v0, v1, v2;
};

void addTriangle(RTCDevice &device, RTCScene &scene)
{
    // Create a new geometry for the triangle
    RTCGeometry mesh = rtcNewGeometry(device, RTC_GEOMETRY_TYPE_TRIANGLE);
    Vertex *vertices = (Vertex *)rtcSetNewGeometryBuffer(
        mesh, RTC_BUFFER_TYPE_VERTEX, 0, RTC_FORMAT_FLOAT3, sizeof(Vertex), 3);

    // Define the vertices
    vertices[0].x = -1.0f;
    vertices[0].y = -1.0f;
    vertices[0].z = -2.0f;
    vertices[1].x = 0.0f;
    vertices[1].y = 1.0f;
    vertices[1].z = -2.0f;
    vertices[2].x = 1.0f;
    vertices[2].y = -1.0f;
    vertices[2].z = -2.0f;

    // Assign the vertices to the geometry buffer
    Triangle *triangles = (Triangle *)rtcSetNewGeometryBuffer(
        mesh, RTC_BUFFER_TYPE_INDEX, 0, RTC_FORMAT_UINT3, sizeof(Triangle), 1);
    triangles[0].v0 = 2;
    triangles[0].v1 = 0;
    triangles[0].v2 = 1;

    // Commit geometry to the mesh
    rtcCommitGeometry(mesh);
    rtcAttachGeometry(scene, mesh);
    rtcReleaseGeometry(mesh);
}

int main()
{
    // Inititiate device and scene
    RTCDevice device = rtcNewDevice("");
    RTCScene scene = rtcNewScene(device);

    // Add geometry to scene
    addTriangle(device, scene);
    rtcCommitScene(scene);

    // Initiate image parameters, image and camera
    const int width = 600;
    const int height = 300;

    std::vector< unsigned char > image;

    embree::Vec3fa lower_left_corner(-2.0f, -1.0f, -1.0f);
    embree::Vec3fa horizontal(4.0f, 0.0f, 0.0f);
    embree::Vec3fa vertical(0.0f, 2.0f, 0.0f);
    embree::Vec3fa origin(0.0f, 0.0f, 0.0f);

    // Render image
    for (int y = height - 1; y >= 0; --y)
    {
        for (int x = 0; x < width; ++x)
        {
            // Initiate pixel colour
            embree::Vec3fa color = embree::Vec3fa(0.0f);

            // Create ray intersection context
            RTCIntersectContext context;
            rtcInitIntersectContext(&context);

            // Generate ray
            float u = float(x) / float(width);
            float v = float(y) / float(height);

            embree::Vec3fa direction(lower_left_corner + horizontal * u +
                                     vertical * v);
            Ray r(origin, normalize(direction), 0.0f, embree::inf);

            // Perform ray intersection
            rtcIntersect1(scene, &context, RTCRayHit_(r));

            // Check if ray intersected any geometry
            if (r.geomID != RTC_INVALID_GEOMETRY_ID)
            {
                color = embree::Vec3fa(1.0f, 1.0f, 1.0f);
            }

            // Set pixel colour
            image.push_back((unsigned char)(color[0] * 255.0f));
            image.push_back((unsigned char)(color[1] * 255.0f));
            image.push_back((unsigned char)(color[2] * 255.0f));
            image.push_back((unsigned char)(255.0f));
        }
    }

    // Output image
    unsigned error = lodepng::encode("hello.png", image, width, height);
    if (error)
        std::cout << "encoder error " << error << ": "
                  << lodepng_error_text(error) << std::endl;

    // Release the scene
    rtcReleaseScene(scene);

    return 0;
}